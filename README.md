## LaTeX thesis template
This is a LaTeX template for theses

Features include:

* lists of figures and tables
* a list of abbreviations (via the glossaries package), with automated handling of when to expand abbreviations
* automated wordcount

To compile, use `./lat thesis_template.tex`

Good luck!

Edward Linscott, Oct 2019
