#!/bin/bash
seed="${@%.*}"

if [ ! -f $seed.tex ]; then echo "Error: $seed.tex does not exist"; exit 2; fi

# Working out which bib tool to use
if [ -f "$seed.bib" ]; then
   echo "$seed.bib not found; continuing without bib"
elif grep -q "backend=biber" "$seed".tex; then
   bib=biber
   bibext=""
elif grep -q "biblio" "$seed".tex; then
   bib=bibtex
   bibext=".aux"
fi
bibcommand="$bib $seed$bibext"

if grep -q "glossaries" "$seed".tex; then
   gls="makeglossaries $seed"
else
   gls=""
fi

# Adding wordcount
wordcount=$(texcount $seed.tex -inc -nobib -total -sum=1,0,1,0,0,0,0 | grep "Sum count" | tail -1 | awk '{ print $3 }') 
echo $wordcount > $seed"_wordcount.tex"

# Typesetting
if [ -n "$bib" ]; then
   pdflatex "$seed".tex && $gls && $bibcommand && pdflatex "$seed".tex && pdflatex "$seed".tex && echo "COMPILED SUCCESSFULLY"
else
   pdflatex "$seed".tex && $gls && echo "COMPILED SUCCESSFULLY (without bib)"
fi
