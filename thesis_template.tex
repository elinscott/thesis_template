% ===========================================================================
%                                 PHD THESIS
% ===========================================================================

% ---------------------------------------------------------------------------
%                                  PREAMBLE
% ---------------------------------------------------------------------------
\documentclass[11pt,twoside,a4paper]{book}

% General %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amsfonts, amsmath, amssymb}  
\usepackage{graphicx}
\usepackage[utf8]{inputenc}

\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\usepackage{setspace}   % allows spacing between lines
%\usepackage{emptypage} % removes numbering AND headers from otherwise empty pages
\usepackage{mathtools}  % allows bold greek symbols, amongst others
\let\epsilon\varepsilon % changes \epsilon to use \varepsilon by default
\usepackage{epstopdf}   % allows reading pdf
\usepackage{layouts}    % allows printing out of widths using \printinunitsof{cm}\prntlen{\length}
\usepackage{enumerate}
\usepackage{xfrac}      % provides \sfrac for slanted fractions
\usepackage[en-US]{datetime2}  % manages date and time formatting
\DTMlangsetup{showdayofmonth=false} % dates will only show <month> <year>
\usepackage{enumitem}   % allows for customisation of enumerate
\usepackage{tocbibind}  % Adds lof/lot to contents without getting page numbers wrong...

% Hyperlinks
% To be able to see hyperlinks (for the purposes of editing)
\usepackage{hyperref}   % turns links into hyperlinks

% To hide hyperlinks (for the final copy)
% \usepackage[hidelinks]{hyperref}   % turns links into hyperlinks
% \hypersetup{colorlinks,
%             linkcolor = black,
%             urlcolor  = black,
%             citecolor = black,
%             anchorcolor = black}

% Captions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[font=small,labelfont=bf,labelsep = colon]{caption} % alters appearance of figure and 
                        % table captions
\usepackage{sidecap}    % allows captions on side of images with the command "\begin{SCfigure}" 
                        % instead of "begin{figure"}
\usepackage{subcaption}

% Floats %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{float}
\usepackage{placeins}   % allows \FloatBarrier

% Figures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The following object allows for figures taken from other papers to be selectively excluded and
% replaced with a placeholder.
% The syntax is \includestolengraphics[width]{path to figure}{height}{fig number in ref.}{ref.}
\newcommand{\includestolengraphics}[5][]{%
  % Uncomment this to use figures
  % \includegraphics[width=#1]{#2}
  % Uncomment this to use placeholders
  \colorbox{gray!30}{%
    \parbox[t][#3][c]{#1}{%
      \centering Figure removed due to copyright. The original can be seen in Ref.~\onlinecite{#5} as Figure #4.
    }
  }
}

% Quotations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{epigraph}   % Allows for the epigraph environment
\usepackage{csquotes}   % Allows for the displayquote environment

% Tables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[flushleft,para]{threeparttable}
\usepackage{tabularx}   % Special column class X (expands to fill space)
\usepackage{array}      % Special column classes m, p (fixed width)
\usepackage{dcolumn}    % Allows creating of decimal-aligned column classes
\newcolumntype{d}{D{.}{.}{-1}} % Simple column class "d" aligned on vertical (taken from revtex 4.1)
                        % with the decimal point centred
\usepackage{multicol}
\usepackage{multirow}

% Colours %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{xcolor}      % allows coloured text via \textcolor{"colour name" e.g. red}{"Lorem ipsum"}
% Custom colours can be defined, e.g.
\definecolor{camred}{RGB}{238, 38, 66}

% Geometry %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{lscape}                     % Allows landscape pages
\usepackage[inner=3cm, outer=2cm]{geometry}
% See https://www.overleaf.com/learn/latex/Page_size_and_margins
\setlength{\topmargin}{-0.54cm + \voffset} % = 1 inch + voffset - 2 cm (resulting in the header being 2cm below the top of the page)
\setlength{\headheight}{12pt} % ~ 4.2mm
\setlength{\headsep}{1.00cm} % Space between header and body
\setlength{\footskip}{24pt} % We don't have a footer, only footnotes, but we need a little padding to be between text and page numbers
\setlength{\textheight}{24.3cm} % = 29.7cm - (2 cm + \headsep + \headheight + 2 cm)

% Bibliography %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[backend=biber, style=nature, maxbibnames=4, maxcitenames=1, autocite=superscript, 
            sorting=none, giveninits=true, isbn=false, doi=false, url=false]{biblatex}
% Note that different styles might not turn [7,3,4,5] into [3-5,7]. Be careful!
\addbibresource{thesis.bib}
\renewbibmacro{in:}{}                           % Remove the "In:"
\DeclareFieldFormat{pages}{\mkfirstpage{#1}} % Only have first page appear
\AtEveryBibitem{%
   \clearfield{shorttitle}%
   \clearfield{month}%
   \clearfield{abstract}%
   }
% Make all authors be listed as F. M. Surname (in conjunction with giveninits=true)
\DeclareNameAlias{default}{first-last}

% An inline citation without square brackets
% (From https://tex.stackexchange.com/questions/25962/biblatex-cite-command-to-create-numeric-citation-without-parentheses)
% A simple modification of code from /usr/local/texlive/2016/texmf-dist/tex/latex/biblatex/cbx/numeric-comp.cbx
\DeclareCiteCommand{\onlinecite}%[\mkbibbrackets]
  {\usebibmacro{cite:init}%
   \usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   \usebibmacro{cite:comp}}
  {}
  {\usebibmacro{cite:dump}%
   \usebibmacro{postnote}}

% Make title of citations a hyperlink
% (From https://tex.stackexchange.com/questions/48400/biblatex-make-title-hyperlink-to-dois-url-or-isbn)
\newbibmacro{string+doiurl}[1]{%
  \iffieldundef{doi}{%
    \iffieldundef{url}{%
        #1%
    }{%
      \href{\thefield{url}}{#1}%
    }%
  }{%
    \href{http://dx.doi.org/\thefield{doi}}{#1}%
  }%
}

\DeclareFieldFormat{howpublished}{\texttt{\url{#1}}}
\DeclareFieldFormat{title}{\usebibmacro{string+doiurl}{\mkbibemph{#1}}}
\DeclareFieldFormat[article,incollection]{title}%
    {\usebibmacro{string+doiurl}{#1}}

% Ensuring \citeauthor is a hyperlink to the bib entry
\DeclareCiteCommand{\citeauthor}
  {\boolfalse{citetracker}%
   \boolfalse{pagetracker}%
   \usebibmacro{prenote}}
  {\ifciteindex
     {\indexnames{labelname}}
     {}%
   \printtext[bibhyperref]{\printnames{labelname}}}
  {\multicitedelim}
  {\usebibmacro{postnote}}

% Ensuring tildes in urls survive
\DeclareSourcemap{
  \maps{
    \map{
      \step[fieldsource=howpublished,
            match=\regexp{\$\\sim\$},
            replace=\regexp{\~}]
    }
    \map{
      \step[fieldsource=url,
            match=\regexp{\$\\sim\$},
            replace=\regexp{\~}]
    }
  }
}

% Biber does weird stuff converting to utf8 -- so make sure we can handle this
\usepackage{newunicodechar}
\newunicodechar{⟨}{\langle}
\newunicodechar{⟩}{\rangle}
\newunicodechar{∞}{$\infty$}
\newunicodechar{−}{--}

% List of Abbreviations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[acronym, nopostdot, nomain, toc, style=alttree]{glossaries}
\renewcommand*{\glsgroupskip}{}          % Don't group acronyms by initial letter
\renewcommand*{\glsclearpage}{}

\input{glossary}

\makeglossaries
\glsfindwidesttoplevelname

% Labelling %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{cleveref}             % Allows reference of multiple equations at once
\newcommand{\crefrangeconjunction}{--}

% Ensuring desired capitalisation
\crefrangeformat{equation}{Equations~#3#1#4--#5#2#6}
\crefmultiformat{equation}{Equations~#2#1#3}{ and~#2#1#3}{, #2#1#3}{ and~#2#1#3}
\crefformat{equation}{Equation~#2#1#3}
\crefrangeformat{figure}{Figures~#3#1#4--#5#2#6}
\crefmultiformat{figure}{Figures~#2#1#3}{ and~#2#1#3}{, #2#1#3}{ and~#2#1#3}
\crefformat{figure}{Figure~#2#1#3}
\crefrangeformat{table}{Tables~#3#1#4--#5#2#6}
\crefmultiformat{table}{Tables~#2#1#3}{ and~#2#1#3}{, #2#1#3}{ and~#2#1#3}
\crefformat{table}{Table~#2#1#3}
\crefname{chapter}{Chapter}{Chapters}
\crefname{section}{Section}{Sections}
\crefname{subsection}{Subsection}{Subsections}
% N.B. appendices must still be manually labelled as "appendix \ref{}" rather than "\cref{}", so if 
% you change the style of the crefnames, make sure to do so for the manual appendix references, too

% Footnotes %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[symbol,perpage,multiple]{footmisc}   % Allows multiple footnotes at the same point simply via \footnote{}\footnote{} (will insert a comma as appropriate)

% Customisation of the order of symbols
\makeatletter
\def\@fnsymbol#1{\ensuremath{\ifcase#1\or \dagger\or \ddagger\or
   \mathsection\or *\or \mathparagraph\or \dagger\dagger
   \or \ddagger\ddagger \or** \else\@ctrerr\fi}}
\makeatother
\renewcommand{\thefootnote}{\fnsymbol{footnote}} % Use symbols instead of numbers, to avoid confusion with citations
\interfootnotelinepenalty=1000000 % this prevents footnotes spanning multiple pages

% For editing
\newcommand{\notetoself}[1]{\textcolor{red}{#1}}
% \newcommand{\notetoself}[1]{} % Uncomment to hide comments

\begin{document}

%TC:ignore

\pagestyle{empty}

% ----------------------------------------------------------------------
%                  TITLE PAGE: name, degree,...
% ----------------------------------------------------------------------
\vspace*{-5mm}

\begin{center}

\begin{huge}
  Your title here\par
\end{huge}

\vspace{40mm} 

\includegraphics[height=4.5cm]{./UoClogo}

\vspace{50mm}

{\Large Your name here}

\vspace{11pt}

Your college here, University of Cambridge

\vspace{10mm}


\footnotesize This thesis is submitted for the degree of...

\vspace{2mm}

\today

\vspace{10mm}

Supervised by... 

\vspace{2mm}

Your department here
\end{center}

%TC:endignore

\normalsize

% Setting up style for preface
\onehalfspacing
\cleardoublepage
\pagenumbering{roman}
\pagestyle{plain}

% -------------------------------------------------------------
% DECLARATION
% -------------------------------------------------------------
%TC:ignore
\setcounter{page}{1}
\input{declaration.tex}
%TC:endignore

\cleardoublepage
% -------------------------------------------------------------
% ABSTRACT
% --------------------------------------------------------------
\input{./abstract}

\cleardoublepage

% --------------------------------------------------------------
% ACKNOWLEDGEMENTS                   
% --------------------------------------------------------------
\input{./acknowledgements}

\cleardoublepage
 
% --------------------------------------------------------------
% CONTENTS
% --------------------------------------------------------------

%TC:ignore

\setcounter{secnumdepth}{2} % organisational level that receives a number
\setcounter{tocdepth}{2}    % print table of contents for level 3
% levels are: 0 - chapter, 1 - section, 2 - subsection, 3 - subsection
\tableofcontents            % print the table of contents

\listoffigures

\listoftables

\cleardoublepage
\phantomsection
\printglossary[type=\acronymtype,title={List of Abbreviations}, nonumberlist]
\cleardoublepage

% -------------------------------------------------------------
% PREFACE    
% -------------------------------------------------------------
\input{preface.tex}
%TC:endignore

% -------------------------------------------------------------
% BLANK PAGES
% -------------------------------------------------------------
\clearpage
\pagestyle{empty}
\cleardoublepage

% --------------------------------------------------------------
% BODY
% --------------------------------------------------------------
\pagestyle{headings}
\setcounter{page}{1}
\pagenumbering{arabic}

\chapter{Introduction}
\label{chapter:1}
\glsresetall
\input{./chapter_1}

\chapter{Chapter with a very very long title that would be bad in the header}
\label{chapter:2}
\chaptermark{Abbrev. title that fits in header}
\input{./chapter_2}

% --------------------------------------------------------------
%                            REFERENCES
% --------------------------------------------------------------
\renewcommand{\bibname}{References} % changes the header; default: Bibliography
\addcontentsline{toc}{chapter}{References}
\printbibliography

%: --------------------------------------------------------------
%:                  APPENDICES
% --------------------------------------------------------------
\renewcommand{\thechapter}{\Alph{chapter}}
\chapter*{Appendices}
\markboth{APPENDICES}{}

% Ensuring a space occurs between chapters in LOF and LOT
\addtocontents{lof}{\vspace{10pt}}
\addtocontents{lot}{\vspace{10pt}}
\addcontentsline{toc}{chapter}{Appendices}
\setcounter{chapter}{1}
\setcounter{section}{0}
\setcounter{subsection}{0}
\setcounter{equation}{0}
\setcounter{footnote}{0}
\setcounter{figure}{0}

\glsresetall % Expand each acronym. This can also be done on a case-by-case basis
\input{./appendix}

% Blank page to finish
\cleardoublepage\pagestyle{empty}\mbox{}
\end{document}
